const {BadRequestError} = require('../../errors');
const {Load} = require('../../models/loadModel');

module.exports.getRelatedLoad = async (req, res, next) => {
  const userId = req.user._id;
  const userRole = req.user.role;
  const loadId = req.params['id'];


  if (!loadId.match(/^[0-9a-fA-F]{24}$/)) {
    throw new BadRequestError('You dont have load with such id');
  }

  let load;
  if (userRole === 'SHIPPER') {
    load = await Load.findOne({created_by: userId, _id: loadId}, {__v: 0});
  } else if (userRole === 'DRIVER') {
    load = await Load.findOne({assigned_to: userId, _id: loadId}, {__v: 0});
  }
  if (!load) {
    throw new BadRequestError('You dont have load with such id');
  }

  req.load = load;
  next();
};
